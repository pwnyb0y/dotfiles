#!/bin/sh
set -o vi

export PATH=$PWD/scripts:$PATH

# Aliases
unalias -a

alias ls='ls --color=auto'
alias grep='grep --color=auto'

alias ll='ls -laF'
alias la='ls -A'
alias l='ls -CF'

# Fast nav to different repos
alias pwnyb0y='cd $HOME/repos/gitlab.com/pwnyb0y'
alias pdev='cd $HOME/repos/gitlab.com/pwnyb0y.dev'
alias jdev='cd $HOME/repos/gitlab.com/jonmarc.dev'
alias _rust='cd $HOME/repos/gitlab.com/_rustbook'


# Search via lynx
alias ?=duck
alias ??=google

alias protonvpn='sudo protonvpn'


bash_prompt_command() {
  # How many characters of the $PWD should be kept
  local pwdmaxlen=40
  # Indicate that there has been dir truncation
  local trunc_symbol=".."
  local dir=${PWD##*/}
  pwdmaxlen=$(( ( pwdmaxlen < ${#dir} ) ? ${#dir} : pwdmaxlen ))
  NEW_PWD=${PWD/#$HOME/\~}
  local pwdoffset=$(( ${#NEW_PWD} - pwdmaxlen ))
  if [ ${pwdoffset} -gt "0" ]
  then
      NEW_PWD=${NEW_PWD:$pwdoffset:$pwdmaxlen}
      NEW_PWD=${trunc_symbol}/${NEW_PWD#*/}
  fi
}

bash_prompt() {
  # Colors
  local NONE="\[\033[0m\]"

  # Regular
  local BK="\033[0;30m\]"
  local RD="\033[0;31m\]"
  local GN="\033[0;32m\]"
  local YL="\033[0;33m\]"
  local BL="\033[0;34m\]"
  local MG="\033[0;35m\]"
  local CA="\033[0;36m\]"
  local WH="\033[0;37m\]"

  # Bold
  local BBK="\033[1;30m\]"
  local BRD="\033[1;31m\]"
  local BGN="\033[1;32m\]"
  local BYL="\033[1;33m\]"
  local BBL="\033[1;34m\]"
  local BMG="\033[1;35m\]"
  local BCA="\033[1;36m\]"
  local BWH="\033[1;37m\]"

  PS1="${debian_chroot:+($debian_chroot)}\[${CA}\${NEW_PWD}
  $WH[${BGN}\u@\h\[${WH}]\$ "

}

PROMPT_COMMAND=bash_prompt_command
bash_prompt
unset bash_prompt

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'
fi
