#!/bin/bash

echo $PATH

ln -sf $PWD/bashconfig.sh $HOME/.bash_aliases
ls -l $HOME/.bash_aliases
ln -sf $PWD/vimrc $HOME/.vimrc
ls -l $HOME/.vimrc
ln -sf $PWD/tmux/tmux.conf $HOME/.tmux.conf
ls -l $HOME/.tmux.conf

ln -sf $PWD/i3/i3config $HOME/.config/i3/config
ls -l $HOME/.config/i3/config
ln -sf $PWD/i3/i3status $HOME/.config/i3status/config
ls -l $HOME/.config/i3status/config
