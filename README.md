## Dot Files

I use i3 for my dekstop environment when I can.

I am working on my VI(M) fluency.

Also working on bash scripting, like the commit script for staging
changes, committing, and pushing.

## Instructions
1. cd to the directory you would like to house the dotfiles in then run `git
clone git@gitlab:pwnyb0y/dotfiles.git` if you wish to clone all of the
dotfiles, or target specific files if you would prefer.
1. cd into the dotfiles folder you just cloned
1. `./setup.sh`

## To do
* Add create bash script to run git init, stage, commit, and push (create
  on gitlab) a new project/repo
* Update setup.sh to include symbolic linking of i3 configs.
* Add install.sh to install the few dependencies required, like rofi for
  the modified i3 dmenu, pandoc and lynx for running livemd markdown
  preview via lynx browser, and any lynx related commands like creating
  short urls for sharing via ix script, etc.

